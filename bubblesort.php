<html>
	<head>
		<link rel="stylesheet" href="./styles/simple.css">
	</head>
	<body>
	<h1>BubbleSort</h1>

<?php
include("./SortAlgorithm.php");
class BubbleSort extends SortAlgorithm
{		
	function sort(&$array)
	{
		$isFinished = true;
		for($i=1;$i<sizeof($array);$i++)
		{
			if($array[$i-1] > $array[$i])
			{
				$swapHelper = $array[$i-1];
				$array[$i-1] = $array[$i];
				$array[$i] = $swapHelper;
				$isFinished = false;
				//$this->algoOutputs .= $swapHelper."<->".$array[$i-1]." ";
				//$this->swapCount++;
			}
		}
		
		if(!$isFinished)
		{
			//$this->algoOutputs .= "\n";
			$this->sort($array);
		}
	}
}

$BubbleSort1 = new BubbleSort();
$BubbleSort1->displayResult();

?>
	</body>
</html>
