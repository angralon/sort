<?php
class Timer
{
	private $laps,$start;
	
	function Timer()
	{
		$this->laps = array();
	}
	
	function start()
	{
		$this->start = microtime(true);
		$this->laps[0] = 0;
	}
	
	function newLap()
	{
		$this->laps[] = microtime(true)-$this->start;
	}
	
	function stop()
	{
		$this->newLap();
	}
	
	function getDifference($index = -1)
	{
		if($index == -1) 
		{
			$stop = end($this->laps);
		}
		else
		{
			$stop = $this->laps[$index];
		}
		return $stop-$this->laps[0];
	}	
	
	function getLaps()
	{
		return $this->laps;
	}
	
	function reset()
	{
		unset($this->laps);
	}
}
?>
