<html>
	<head>
		<link rel="stylesheet" href="./styles/simple.css">
	</head>
	<body>
	<h1>QuickSort</h1>

<?php 
include("./SortAlgorithm.php");
class QuickSort extends SortAlgorithm
{	 

 	function sort(&$array)
 	{
 		$this->qSort(0,sizeof($array)-1,$array);
 	}
 
 	function qSort($left,$right,&$array)
 	{
 		if($left < $right)
 		{
 			//$this->algoOutputs .= "\n\n".$this->returnArray($array);
 			//$this->algoOutputs .= "\n";
 			$pivot = $this->split($left,$right,$array);
 			$this->qSort($left, $pivot-1, $array);
 			$this->qSort($pivot+1, $right, $array);
 		}
 	}
 	
 	function split($left,$right,&$array)
 	{
 		$i = $left;
 		$j = $right-1;
 		$pivot = $array[$right];
 		
 		while($i < $j)
 		{
 			while($array[$i] <= $pivot && $i < $right)
 			{
 				$i++;
 			}
 			
 			while($array[$j] >= $pivot && $j > $left)
 			{
 				$j--;
 			}
 			
 			if($i < $j)
 			{
 				list($array[$i], $array[$j]) = array($array[$j], $array[$i]); // swap elements
				//$this->algoOutputs .= $array[$j]."<->".$array[$i]." ";
				$this->swapCount++;
 			}
 		}
 		
 		if($array[$i] > $pivot)
 		{
 			list($array[$i], $array[$right]) = array($array[$right], $array[$i]); // swap elements
			//$this->algoOutputs .= $array[$right]."<->".$array[$i]." ";
			$this->swapCount++;
 		}
 		return $i;
 	}
}

$QuickSort1 = new QuickSort();
$QuickSort1->displayResult();

?>
	</body>
</html>
