<?php

class SortAlgorithm
{
	protected $ARRAY_SIZE;
	protected $NUMBER_RANGE;
	protected $numbers;
	protected $algoOutputs;
	protected $swapCount;
	protected $timer;
	
	function SortAlgorithm()
	{
		require_once("./timer.php");
		$this->ARRAY_SIZE = 1000; 
		$this->NUMBER_RANGE = 99;
		$this->numbers = array();
		$this->generateNumberArray();
		$this->algoOutputs = "";
		$this->swapCount = 0;
		$this->timer = new Timer();
	}
	
	function generateNumberArray()
	{
		$i = 0;
		while($i < $this->ARRAY_SIZE)
		{
			$this->numbers[$i] = rand(0,$this->NUMBER_RANGE);
			$i++;
		}
	}
	
	function returnArray(&$array, $separator=", ")
	{
		$i = 0;
		$returnString = "";
		while($i < sizeof($array))
		{
			$returnString.= $array[$i];

			if(!($i+1 == sizeof($array)))
			{
				$returnString.= $separator;
			}
			
			$i++;
		}
		return $returnString;
	}
	
	function sort(&$array)
	{
		sort($array);
	}
	
	function displayResult()
	{
		$echo = "";
		$echo .= "<h3>Unsorted Array:</h3>";
		$echo .= $this->textArea($this->returnArray($this->numbers));
		$echo .= "<h3>Sorted Array: </h3>";
		$this->timer->start();
		$this->sort($this->numbers);
		$this->timer->newLap();
		$echo .= $this->textArea($this->returnArray($this->numbers));
		$this->timer->newLap();
		$echo .= "<p>Array size: ".$this->ARRAY_SIZE."</p>";
		$echo .= "<p>Number range: ".$this->NUMBER_RANGE."</p>";
		$echo .= "<p>Swap count: ".$this->swapCount."</p>";
		$echo .= "<h3>Algorithm Outputs:</h3>";
		$this->timer->newLap();
		$echo .= $this->textArea($this->algoOutputs);
		$echo .= "<p>Runtime in seconds: ".$this->returnArray($this->timer->getLaps()," - ")."</p>";
		$this->timer->reset();
		$this->timer->start();
		$this->timer->newLap();
		$echo .= "<p>Reset array: ".$this->returnArray($this->timer->getLaps()," - ")."</p>";
		
		
		echo $echo;	
	}
	
	function textArea($string)
	{
		return "<textarea readonly=\"readonly\">".$string."</textarea>";
	}
}
?>
