<html>
	<head>
		<link rel="stylesheet" href="./styles/simple.css">
	</head>
	<body>
	<h1>PHP sort($array)</h1>

<?php
include("./SortAlgorithm.php");
class PHPSort extends SortAlgorithm
{	
	function sort(&$array)
	{
		$this->algoOutputs .= "success: ".sort($array);
	}
}

$PHPSort1 = new PHPSort();
$PHPSort1->displayResult();

?>
	</body>
</html>
