<?php
$SA = new SortAlgorithm();
$SA->generateNumberArray();
$SA->displayResult();

class SortAlgorithm
{
	private $ARRAY_SIZE;
	private $NUMBER_RANGE;
	private $numbers;
	private $algoOutputs;
	private $swapCount;
	private $microTime;
	
	function SortAlgorithm()
	{
		$this->ARRAY_SIZE = 50000; 
		$this->NUMBER_RANGE = 99;
		$this->numbers = array();
		//$this->algoOutputs = "";
		$this->swapCount = 0;
	}
	
	function generateNumberArray()
	{
		$i = 0;
		while($i < $this->ARRAY_SIZE)
		{
			$this->numbers[$i] = rand(0,$this->NUMBER_RANGE);
			$i++;
		}
	}
	
	function returnArray($array)
	{
		$i = 0;
		$returnString = "";
		while($i < sizeof($array))
		{
			$returnString.= $array[$i].",";
/*
			if(!($i+1 == sizeof($array)))
			{
				$returnString.= ",";
			}*/
			
			$i++;
		}
		return $returnString;
	}
	
	function sort($array)
 	{
 		$this->qSort(0,sizeof($array)-1,$array);
 	}
 
 	function qSort($left,$right)
 	{
 		if($left < $right)
 		{
 			//$this->algoOutputs .= "\n\n".$this->returnArray($this->numbers);
 			//$this->algoOutputs .= "\n";
 			$pivot = $this->split($left,$right);
 			$this->qSort($left, $pivot-1);
 			$this->qSort($pivot+1, $right);
 		}
 	}
 	
 	function split($left,$right)
 	{
 		$i = $left;
 		$j = $right-1;
 		$pivot = $this->numbers[$right];
 		
 		while($i < $j)
 		{
 			while($this->numbers[$i] <= $pivot && $i < $right)
 			{
 				$i++;
 			}
 			
 			while($this->numbers[$j] >= $pivot && $j > $left)
 			{
 				$j--;
 			}
 			
 			if($i < $j)
 			{
 				list($this->numbers[$i], $this->numbers[$j]) = array($this->numbers[$j], $this->numbers[$i]); // swap elements
				//$this->algoOutputs .= $this->numbers[$j]."<->".$this->numbers[$i]." ";
				$this->swapCount++;
 			}
 		}
 		
 		if($this->numbers[$i] > $pivot)
 		{
 			list($this->numbers[$i], $this->numbers[$right]) = array($this->numbers[$right], $this->numbers[$i]); // swap elements
			//$this->algoOutputs .= $this->numbers[$right]."<->".$this->numbers[$i]." ";
			$this->swapCount++;
 		}
 		return $i;
 	}
	
	function displayResult()
	{
		$echo = "";
		$echo .= "<h3>Unsorted Array:</h3>";
		$echo .= $this->textArea($this->returnArray($this->numbers));
		$echo .= "<h3>Sorted Array: </h3>";
		$this->saveMicrotime();
		$this->sort($this->numbers);
		$echo .= $this->textArea($this->returnArray($this->numbers));
		$echo .= "<p>Array size: ".$this->ARRAY_SIZE."</p>";
		$echo .= "<p>Number range: ".$this->NUMBER_RANGE."</p>";
		$echo .= "<p>Swap count: ".$this->swapCount."</p>";
		$echo .= "<p>Runtime in seconds: ".round($this->getMicroDifference(),8);
		$echo .= "<h3>Algorithm Outputs:</h3>";
		//$echo .= $this->textArea($this->algoOutputs);
		
		echo $echo;	
	}
	
	function textArea($string)
	{
		//return $string;
		return "<textarea readonly=\"readonly\">".$string."</textarea>";
	}
	
	function saveMicrotime()
	{
		$this->microTime = microtime(true);
	}
	
	function getMicroDifference()
	{
		return microtime(true)-$this->microTime;
	}
}
?>
