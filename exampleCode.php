<?php 
function quicksort($seq) {
    if(!count($seq)) return $seq;
    $pivot= $seq[0];
    $low = $high = array();
    $length = count($seq);
    for($i=1; $i < $length; $i++) {
        if($seq[$i] <= $pivot) {
            $low [] = $seq[$i];
        } else {
            $high[] = $seq[$i];
        }
    }
    return array_merge(quicksort($low), array($pivot), quicksort($high));
}

function quicksort2($array) {
  if (count($array) <= 1) {
    return $array;
  }
 
  $pivot_value = array_shift($array);
 
  return array_merge(
    quicksort2(array_filter($array, function ($v) use($pivot_value) {return $v < $pivot_value;})),
    array($pivot_value),
    quicksort2($higher = array_filter($array, function ($v) use($pivot_value) {return $v >= $pivot_value;}))
  );
}

function generateNumberArray()
	{
		$array = array();
		$i = 0;
		while($i < 50000)
		{
			$array[$i] = rand(0,99);
			$i++;
		}
		return $array;
	}


//Let's try some examples

$myarr = generateNumberArray();
$micro = microtime(true);
echo $micro."<br><textarea>";
$final = quicksort2($myarr);
$micro = microtime(true) - $micro;
print_r($final);
echo "</textarea><br>".$micro;

?>
